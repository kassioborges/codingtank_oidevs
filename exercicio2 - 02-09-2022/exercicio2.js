//Escreva um algoritmo para ler as dimensões de um retângulo (base e altura), 
//calcular e escrever a área do retângulo.

var base = 5;
var altura = 10;

const area = base * altura;

console.log("A área do retângulo é " + area);

//Ler um valor e escrever se é positivo, negativo ou zero

var numero = 10;

if (numero > 0) {
  console.log("O número " + numero + " é positivo");
} else if (numero < 0) {
  console.log("O número " + numero + " é negativo");
} else {
  console.log("O número é zero");
}

//Escreva um programa que leia um número e imprima se este número é ou não par.
var numb = 20;

if (numb%2 === 1) {
  console.log("O número " + numb + " é ímpar");
} else {
  console.log("O número " + numb + " é par");
}