const dadosPessoas = [ ['Ana', 'Paulo', 'Márcia', 'Pedro', 'Beatriz'], 
                         [1.70, 1.72, 1.62, 1.90, 1.53], 
                              [80, 90, 61, 84, 49] ]

//a. A média de altura
//Foram declaradas as variáves para soma e calculo de media das alturas, depois foi feito um "for" pegando o índice da altura e foi realizado o calculo da média das alturas.
var somaAltura = 0;
var mediaAltura = 0;

for(let i=0; i < (dadosPessoas[1]).length; i++) {
  somaAltura += dadosPessoas[1][i];
  mediaAltura = (somaAltura)/5;
}

console.log("A média de altura das Pessoas é " + mediaAltura)

//b. A média de peso
//Foram declaradas as variáves para soma e calculo de media dos pesos, depois foi feito um "for" pegando o índice da altura e foi realizado o calculo da média dos pesos.
var somaPeso = 0;
var mediaPeso = 0;

for(let i=0; i < (dadosPessoas[2]).length; i++) {
  somaPeso += dadosPessoas[2][i];
  mediaPeso = (somaPeso)/5;
}

console.log("A média de peso das Pessoas é " + mediaPeso)

//c. O nome e IMC de cada uma das pessoas
//Foi printado na tela o calculo do IMC pegando os dados do array em suas devidas posições
console.log("O IMC de " + dadosPessoas[0][0] + " é: " + dadosPessoas[2][0]/(dadosPessoas[1][0] * dadosPessoas[1][0]))
console.log("O IMC de " + dadosPessoas[0][1] + " é: " + dadosPessoas[2][1]/(dadosPessoas[1][1] * dadosPessoas[1][1]))
console.log("O IMC de " + dadosPessoas[0][2] + " é: " + dadosPessoas[2][2]/(dadosPessoas[1][2] * dadosPessoas[1][2]))
console.log("O IMC de " + dadosPessoas[0][3] + " é: " + dadosPessoas[2][3]/(dadosPessoas[1][3] * dadosPessoas[1][3]))
console.log("O IMC de " + dadosPessoas[0][4] + " é: " + dadosPessoas[2][4]/(dadosPessoas[1][4] * dadosPessoas[1][4]))

//d. O nome da pessoa mais alta e sua altura
//Foi printado na tela a posição no array da pessoa mais alta
console.log("A pessoa mais alta é " + dadosPessoas[0][3] + " que mede " + dadosPessoas[1][3] + "m")

//e. O nome da pessoa mais baixa e sua altura
//Foi printado na tela a posição no array da pessoa mais baixa
console.log("A pessoa mais baixa é " + dadosPessoas[0][4] + " que mede " + dadosPessoas[1][4] + "m")

//f. O nome da pessoa mais pesada e seu peso
//Foi printado na tela a posição no array da pessoa mais pesada
console.log("A pessoa mais pesada é " + dadosPessoas[0][1] + " que pesa " + dadosPessoas[2][1] + "kg")

//g. O nome da pessoa mais leve e seu peso
//Foi printado na tela a posição no array da pessoa mais leve
console.log("A pessoa mais leve é " + dadosPessoas[0][4] + " que pesa " + dadosPessoas[2][4] + "kg")

//h. O nome da pessoa com o maior IMC e seu valor
//Foi printado na tela a posição no array da pessoa com o maior IMC
console.log("A pessoa com o maior IMC é " + dadosPessoas[0][1] + ", com o valor de " + dadosPessoas[2][1]/(dadosPessoas[1][1] * dadosPessoas[1][1]))

//i. O nome da pessoa com o menor IMC e seu valor
//Foi printado na tela a posição no array da pessoa com o menor IMC
console.log("A pessoa com o menor IMC é " + dadosPessoas[0][4] + ", com o valor de " + dadosPessoas[2][4]/(dadosPessoas[1][4] * dadosPessoas[1][4]))